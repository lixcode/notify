#include "notify.h"

#include <Foundation/Foundation.h>
#include <objc/runtime.h>

#include <QDebug>
#include <QTimer>
#include <QtMac>

#define VERSION2 version.minorVersion >= 14
#define VERSION1 version.minorVersion >= 10

NSOperatingSystemVersion version =
  [[NSProcessInfo processInfo] operatingSystemVersion];

// Step 1: Override bundle interface

@implementation NSBundle (swizle)

// Set up terminal icon if app has no one own
- (NSString *)__bundleIdentifier {
    if (self == [NSBundle mainBundle] && ![[self __bundleIdentifier] length]) {
        return @"com.apple.terminal";
    }
    else {
        return [self __bundleIdentifier];
    }
}

@end

// Replace the implementation with this one
BOOL installNSBundleHook() {
    Class cls = objc_getClass("NSBundle");

    if (cls) {
        method_exchangeImplementations(
          class_getInstanceMethod(cls, @selector(bundleIdentifier)),
          class_getInstanceMethod(cls, @selector(__bundleIdentifier)));

        return YES;
    }
    return NO;
}

// Step 2: Enable feedback

@interface UserNSNotificationItem
    : NSObject <NSUserNotificationCenterDelegate> {
}

- (BOOL)UserNSNotificationCenter:(NSUserNotificationCenter *)center
       shouldPresentNotification:(NSUserNotification *)notification;

@end

@implementation UserNSNotificationItem

- (BOOL)UserNSNotificationCenter:(NSUserNotificationCenter *)center
       shouldPresentNotification:(NSUserNotification *)notification {
    return YES;
}

- (void)UserNSNotificationCenter:(NSUserNotificationCenter *)center
        didActivatedNotification:(NSUserNotification *)notification {
    int id = [notification.identifier intValue];

    if (
      notification.activationType ==
      NSUserNotificationActivationTypeContentsClicked) {
        Notify::instance->actionInvoked(id, "default");
    }
    else if (
      notification.activationType ==
      NSUserNotificationActivationTypeActionButtonClicked) {
        if (Notify::notifications.contains(id))
            Notify::instance->actionInvoked(
              id, Notify::notifications[id].actions[0].first);
    }

    // The notification are not deleted automatically on OS X
    [center removeDeliveredNotification:notification];
    [notification release];
}
@end

@interface UserUNNotificationCenter
    : NSObject <UNUserNotificationCenterDelegate> {
}
@end

@implementation UserUNNotificationCenter

- (void)userNotificationCenter:(UNUserNotificationCenter *)center
       willPresentNotification:(UNNotification *)notification
         withCompletionHandler:
           (void (^)(UNNotificationPresentationOptions options))
             completionHandler {
    [completionHandler:UNNotificationPresentationOptionAlert];
}

- (void)userNotificationCenter:(UNUserNotificationCenter *)center
  didReceiveNotificationResponse:(UNNotificationResponse *)response
           withCompletionHandler:(void (^)(void))completionHandler {
    NSString * idStr = response.notificaton.request.identifier;
    int        id    = [idStr intValue];

    if (response.actionIdentifier == UNNotificationDefaultActionIdentifier) {
        Notify::instance->actionInvoked(id, "default");
    }
    else if (
      response.actionIdentifier == UNNotificationDismissActionIdentifier) {
        Notify::instance->notificationClosed(id, 0);
    }
    else {
        Notify::instance->actionInvoked(id, response.actionIdentifier);
    }

    [center removeDeliveredNotificationsWithIdentifiers:@[idStr]];
    [completionHandler];
}

- (void)userNotificationCenter:(UNUserNotificationCenter *)center
   openSettingsForNotification:(UNNotification *)notification {

    NSString * idStr = notificaton.request.identifier;
    int        id    = [idStr intValue];

    Notify::instance->notificationClosed(id, 0);
    [center removeDeliveredNotificationsWithIdentifiers:@[idStr]];
    qDebug() << "Request to open setting, but not supported";
    return;
}

// Step 3: Define Notify class

namespace backend {
int count = 0;
}

Notify::Notify(const QString & appName, QObject * parent)
    : QObject(parent)
    , appName(appName) {
    instance = this;

    if (VERSION2) {
        item = [UserUNNotificationItem alloc];
        [[UNUserNotificationCenter defaultNotificationCenter] setDelegate:item];
    }
    else if (VERSION1) {
        item = [UserNSNotificationCenter alloc];
        [[NSUserNotificationCenter defaultNotificationCenter] setDelegate:item];
    }
}

Notify::~Notify() {
    if (VERSION2) {
        [[UNUserNotificationCenter defaultNotificationCenter] setDelegate:nil];
    }
    else if (VERSION1) {
        [[NSUserNotificationCenter defaultNotificationCenter] setDelegate:nil];
    }
    [item release];
};

uint Notify::pushNotification(const Notification & notif) {
    int id = ++backend::count;

    NSUserNotification * userNotif = [[NSUserNotification alloc] init];
    NSString *           notifId   = QString::number(id).toNSString();

    if (VERSION2) {
        UNNotificationContent * content = [UNNotificationContent alloc];

        content.title              = notif.header.toNSString();
        content.subtitle           = notif.subheader.toNSString();
        content.body               = notif.text.toNSString();
        content.categoryIdentifier = appName.toNSString();
        content.threadIdentifier   = notif.typeId.toNSString();

        if (notif.silent)
            content.sound =
              [UNNotificationSound defaultCriticalSoundWithAudioVolume:@0.0];

        UNNotificationRequest * userNotif =
          [UNNotificationRequest requestWithIdentifier:notifId, content, nil];

        [[UNUserNotificationCenter currentNotificationCenter]
          addNotificationRequest:userNotif, ^(NSError * error) {
            qDebug() << "notify creation error code" << error.code;
          }];
    }
    else if (VERSION1) {
        userNotif.identifier      = notifId;
        userNotif.title           = notif.header.toNSString();
        userNotif.subtitle        = notif.subheader.toNSString();
        userNotif.informativeText = notif.text.toNSString();

        if (!notif.contentIconImage.isNull())
            userNotif.contentImage =
              QtMac::toNSImage(QPixmap::fromImage(notif.contentIconImage));

        if (!notif.actions.isEmpty()) {
            userNotif.hasActionButton = YES;
            userNotif.actionButtonTitle =
              notif.actions.first().second.toNSString();

            if (notif.actions.begin() != notif.actions.end()) {
                userNotif.otherButtonTitle =
                  notif.actions.last().second.toNSString();
            }
        }

        userNotif.hasReplyButton = NO;

        notifications[id]    = notif;
        notifications[id].ns = userNotif;

        [[NSUserNotificationCenter defaultUserNotificationCenter]
          deliverNotification:userNotif];
    }

    QTimer::singleShot(300000, [this, id]() { this->hideNotification(id); });

    return id;
}

uint Notify::pushAttach(const Notification & notif) {
    Q_UNUSED(notif)
    return 0;
}

void Notify::hideNotification(uint id) {
    if (!notifications.contains(id))
        return;


    if (VERSION2) {
        NSString * notifId = QString::number(id).toNSString();

        [[UNUserNotificationCenter currentNotificationCenter]
          removeDeliveredNotificationsWithIdentifiers:@[notifId]];
    }
    else if (VERSION1) {
        auto userNotif = notifications[id].ns;

        [[NSUserNotificationCenter defaultUserNotificationCenter]
          removeDeliveredNotification:userNotif];
    }

    notifications.remove(id);
}

void Notify::clear() {

    if (VERSION2) {
        [[UNUserNotificationCenter currentNotificationCenter]
          removeAllDeliveredNotifications];
    }
    else if (VERSION1) {
        [[NSUserNotificationCenter defaultUserNotificationCenter]
          removeAllDeliveredNotifications];
    }

    notifications.clear();
}
