#include "notify.h"
#include "wintoastlib.h"

using namespace WinToastLib;

// Step 1: Define handler

class NotifyHandler : public IWinToastHandler
{
public:
    NotifyHandler(uint id, Notify * notify) {
        this->notifyId = id;
        this->notify   = notify;
    }

    void toastActivated() const override {
        notify->actionInvoked(notifyId, "default");
    }

    void toastActivated(int action) const override {
        notify->actionInvoked(notifyId, action);
    }

    void toastFailed() const override {
        std::cout << "Error showing current toast" << std::endl;
    }

    void toastDismissed(WinToastDismissalReason) const override {
        notify->notificationClosed(notifyId, 0);
    }

private:
    uint     notifyId;
    Notify * notify;
};

// Step 2: Define static backend

namespace backend {

static bool inited = false;
static uint count  = 0;

void init(const std::wstring & appName) {
    inited = true;

    auto wt = WinToast::instance();

    wt->setAppName(appName);
    wt->setAppUserModelId(WinToast::configureAUMI(appName, appName));

    if (!wt->initialize()) {
        std::cout << "The system doesn't support toasts" << std::endl;
    }
}

}  // namespace backend

// Step 3: Define class members

Notify::Notify(const QString & appName, QObject * parent)
    : QObject(parent)
    , appName(appName) {
    if (!backend::inited)
        backend::init(appName.toStdWString());

    m_supportPersistence = true;
    m_supportSound       = WinToast::instance()->isSupportingModernFeatures();
}

Notify::~Notify() = default;

uint Notify::pushNotification(const ::Notification & notif) {
    if (!backend::inited)
        backend::init(appName.toStdWString());

    WinToastTemplate tpl;

    if (notif.subheader.isNull()) {
        tpl = WinToastTemplate(WinToastTemplate::ImageAndText02);
        tpl.setTextField(
          notif.text.toStdWString(), WinToastTemplate::SecondLine);
    }
    else {
        tpl = WinToastTemplate(WinToastTemplate::ImageAndText04);
        tpl.setTextField(
          notif.subheader.toStdWString(), WinToastTemplate::SecondLine);
        tpl.setTextField(
          notif.text.toStdWString(), WinToastTemplate::ThirdLine);
    }

    tpl.setTextField(notif.header.toStdWString(), WinToastTemplate::FirstLine);

    if (notif.winIcon.isValid())
        tpl.setImagePath(notif.winIcon.toLocalFile().toStdWString());

    if (notif.silent)
        tpl.setAudioOption(WinToastTemplate::Silent);

    if (notif.resident)
        tpl.setExpiration(300000);

    for (auto it = notif.actions.begin(); it != notif.actions.end(); it++) {
        tpl.addAction(it.value().toStdWString());
    }

    uint id      = ++backend::count;
    auto handler = new NotifyHandler(id, this);

    notifications[id] = notif;
    notifications[id].winToastId =
      WinToast::instance()->showToast(tpl, handler);

    return id;
}

uint Notify::pushAttach(const ::Notification & /*notif*/) {
    return 0;
}

void Notify::hideNotification(uint id) {
    WinToast::instance()->hideToast(notifications[id].winToastId);

    notificationClosed(id, 0);
}


void Notify::clear() {
    WinToast::instance()->clear();
}

void Notify::actionInvoked(uint id, int action) {
    auto    notif     = notifications[id];
    QString actionStr = "default";
    int     actionId  = 0;

    for (auto it = notif.actions.begin(); it != notif.actions.end();
         it++, action++) {
        if (actionId == action) {
            actionStr = it.key();
        }
    }

    actionInvoked(id, actionStr);
}

void Notify::actionInvoked(uint id, const QString & action) {
    if (!notifications.contains(id))
        return;

    if (action == "default") {
        emit clicked(notifications[id]);
    }
    else {
        emit triggered(notifications[id], action);
    }

    notifications.remove(id);
}
void Notify::notificationClosed(uint id, uint /*reason*/) {
    if (!notifications.contains(id))
        return;

    emit closed(notifications[id]);
}
