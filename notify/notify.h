#pragma once

#include "global.h"

#include <QImage>
#include <QMap>
#include <QObject>
#include <QString>
#include <QUrl>

#if defined(Q_OS_LINUX) || defined(Q_OS_FREEBSD)
#include <QtDBus/QDBusArgument>
#include <QtDBus/QDBusInterface>
#endif

#ifdef Q_OS_OSX
#include <Foundation/Foundation.h>
#include <objc/runtime.h>
#endif

struct LIBNOTIFYSHARED_EXPORT Notification
{
    QString typeId;
    QString eventId;
    QString header;
    QString subheader;
    QString text;

    QUrl    winIcon;
    QString unixIcon;

    QImage  contentIconImage;
    QString contentIconPath;

    QUrl attach;
    QUrl attachPreview;

    bool resident = false;
    bool silent   = false;

    QList<std::pair<QString, QString>> actions;

#ifdef Q_OS_WIN
    int64_t winToastId;
#endif

#ifdef Q_OS_OSX
    NSUserNotification * ns;
#endif
};

class LIBNOTIFYSHARED_EXPORT Notify : public QObject
{
    Q_OBJECT
public:
    Notify(const QString & appName, QObject * parent = nullptr);
    ~Notify();

    uint pushNotification(const Notification & notif);

    uint pushAttach(const Notification & notif);

    void hideNotification(uint id);

    void clear();

    bool supportHyperlinks();
    bool supportBodyImages();
    bool supportBodyMark();
    bool supportPersistence();
    bool supportSound();

signals:
    void clicked(const Notification & notif);
    void closed(const Notification & notif);
    void triggered(const Notification & notif, const QString & action);

private:
#if defined(Q_OS_LINUX) || defined(Q_OS_FREEBSD)
    QDBusInterface dbus;

    uint showNotification(
      const QString summary, const QString text, const Notification & notif);

    QStringList parseActions(const Notification & notif);
#endif

#ifdef Q_OS_OSX
    NSObject * item;
#endif

public:
    /// @brief notification ID to Notification
    static QMap<uint, Notification> notifications;

    static Notify * instance;

private:
    /// @brief name of the application
    QString appName = "icL-Notify";

    /// @brief notification timeout in ms
    int timeout = 5000;

    bool m_supportHyperlinks  = false;
    bool m_supportBodyImages  = false;
    bool m_supportBodyMark    = false;
    bool m_supportPersistence = false;
    bool m_supportSound       = false;

public:
#ifdef Q_OS_WIN
    void actionInvoked(uint id, int action);
#endif

    // these slots are platform specific (D-Bus only)
    // but Qt slot declarations can not be inside an ifdef!
public slots:
    void actionInvoked(uint id, const QString & action);
    void notificationClosed(uint id, uint reason);
};

#if defined(Q_OS_LINUX) || defined(Q_OS_FREEBSD)
QDBusArgument &       operator<<(QDBusArgument & arg, const QImage & image);
const QDBusArgument & operator>>(const QDBusArgument & arg, QImage &);
#endif
