#include "notify.h"

QMap<uint, Notification> Notify::notifications = {};

Notify * Notify::instance = nullptr;

bool Notify::supportHyperlinks() {
    return m_supportHyperlinks;
}
bool Notify::supportBodyImages() {
    return m_supportBodyImages;
}
bool Notify::supportBodyMark() {
    return m_supportBodyMark;
}
bool Notify::supportPersistence() {
    return m_supportPersistence;
}
bool Notify::supportSound() {
    return m_supportSound;
}
