#include <QGuiApplication>
#include <QThread>
#include <QTimer>

#include <ownnotify.h>

int main(int argc, char *argv[])
{
    QGuiApplication a(argc, argv);

    OwnNotify man;

//    man.postNotification("Room", "Event", "ChatName", {}, "Text", QImage(":/imgs/user.svg"), QUrl());

    man.pushMessage("User", "ev", "User", "Message");
    man.pushGroupMessage("User", "ev", "Group", "User", "Message");
    man.pushCommunityMessage("User", "ev", "Community", "Group", "User", "Message");
    man.pushGroupAdded("User", "ev", "Group", "User");
    man.pushCommunityAdded("User", "ev", "Community", "Group", "User");

//    QThread::msleep(1000);
//    man.clear();

    a.connect(&man, &Notify::clicked, &a, QGuiApplication::quit);

    QTimer::singleShot(100000, &a, QGuiApplication::quit);

    return a.exec();
}
