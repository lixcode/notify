QT += core gui network

unix:!mac {
    QT += dbus
}

mac {
    QT += macextras
}

CONFIG += c++11 console
CONFIG -= app_bundle

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

HEADERS += notify/notify.h \
    ownnotify.h

SOURCES += \
        main.cpp \
    ownnotify.cpp

mac {
    QMAKE_LFLAGS += -framework Foundation -framework Cocoa
    SOURCES += src/notifications/notifymac.mm
}
else:win32 {
    HEADERS += notify/wintoastlib.h
    SOURCES += notify/notifywin.cpp \
        notify/wintoastlib.cpp
}
else {
    SOURCES += notify/notifylinux.cpp
}

SOURCES += notify/notify.cpp

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    imgs.qrc
