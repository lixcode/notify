#ifndef OWNNOTIFY_H
#define OWNNOTIFY_H

#include "notify/notify.h"

class OwnNotify : public Notify
{
public:
    OwnNotify();

    void pushMessage(
      const QString & chatId, const QString & eventId, const QString & user,
      const QString & message);

    void pushGroupMessage(
      const QString & chatId, const QString & eventId, const QString & group,
      const QString & user, const QString & message);

    void pushCommunityMessage(
      const QString & chatId, const QString & eventId,
      const QString & community, const QString & group, const QString & user,
      const QString & message);

    void pushGroupAdded(
      const QString & chatId, const QString & eventId, const QString & group,
      const QString & user);

    void pushCommunityAdded(
      const QString & chatId, const QString & eventId,
      const QString & community, const QString & group, const QString & user);

    void pushActions(const QString & chatId, const QString & eventId, const QString & header,
      const QString & body, const QList<std::pair<QString, QString> > &actions);

    void pushImage1(
      const QString & chatId, const QString & eventId, const QString & header,
      const QString & body, const QString & src, const QString & alt);


private:
    QString winicon  = "file:///C:/Users/Win10/Downloads/icon.png";
    QString unixIcon = "folder-open";
};

#endif  // OWNNOTIFY_H
