#include "ownnotify.h"

#include <QStringBuilder>

OwnNotify::OwnNotify()
    : Notify("Twips") {}

void OwnNotify::pushMessage(
  const QString & chatId, const QString & eventId, const QString & user,
  const QString & message) {
    Notification notif;

    notif.typeId   = chatId;
    notif.eventId  = eventId;
    notif.header   = user;
    notif.text     = message;
    notif.winIcon  = winicon;
    notif.unixIcon = unixIcon;

    pushNotification(notif);
}

void OwnNotify::pushGroupMessage(
  const QString & chatId, const QString & eventId, const QString & group,
  const QString & user, const QString & message) {
    Notification notif;

    notif.typeId    = chatId;
    notif.eventId   = eventId;
    notif.header    = group;
    notif.subheader = user % " has sended you a message";
    notif.text      = message;
    notif.winIcon   = winicon;
    notif.unixIcon  = unixIcon;

    pushNotification(notif);
}

void OwnNotify::pushCommunityMessage(
  const QString & chatId, const QString & eventId, const QString & community,
  const QString & group, const QString & user, const QString & message) {
    Notification notif;

    notif.typeId    = chatId;
    notif.eventId   = eventId;
    notif.header    = group % " of " % community;
    notif.subheader = user % " has sended you a message";
    notif.text      = message;
    notif.winIcon   = winicon;
    notif.unixIcon  = unixIcon;

    pushNotification(notif);
}

void OwnNotify::pushGroupAdded(
  const QString & chatId, const QString & eventId, const QString & group,
  const QString & user) {
    Notification notif;

    notif.typeId    = chatId;
    notif.eventId   = eventId;
    notif.header    = group;
    notif.subheader = user % " has added you to chat of group";
    notif.text      = "Press to see!";
    notif.winIcon   = winicon;
    notif.unixIcon  = unixIcon;

    pushNotification(notif);
}

void OwnNotify::pushCommunityAdded(
  const QString & chatId, const QString & eventId, const QString & community,
  const QString & group, const QString & user) {
    Notification notif;

    notif.typeId    = chatId;
    notif.eventId   = eventId;
    notif.header    = group % " of " % community;
    notif.subheader = user % " has added you to chat of group";
    notif.text      = "Press to see!";
    notif.winIcon   = winicon;
    notif.unixIcon  = unixIcon;

    pushNotification(notif);
}

void OwnNotify::pushActions(
  const QString & chatId, const QString & eventId, const QString & header,
  const QString & body, const QList<std::pair<QString, QString>> & actions) {
    Notification notif;

    notif.typeId   = chatId;
    notif.eventId  = eventId;
    notif.header   = header;
    notif.text     = body;
    notif.winIcon  = winicon;
    notif.unixIcon = unixIcon;
    notif.actions  = actions;
    notif.resident = true;

    pushNotification(notif);
}

void OwnNotify::pushImage1(
  const QString & chatId, const QString & eventId, const QString & header,
  const QString & body, const QString & src, const QString & alt) {
    Notification notif;

    notif.typeId    = chatId;
    notif.eventId   = eventId;
    notif.header    = header;
    notif.subheader = alt;
    notif.text      = body;
    notif.winIcon   = winicon;
    notif.unixIcon  = unixIcon;
    notif.attach    = src;

    pushAttach(notif);
}
